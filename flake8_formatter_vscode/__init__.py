"""Flake8 formatter."""

from .plugin import VscodePathFormatter

__all__ = ("VscodePathFormatter",)
__version__ = "0.0.1"
